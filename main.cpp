#include "headers/globals.h"
#include "headers/inits.h"
#include "threads/ClientThread.h"
#include "threads/CookThread.h"
#include "threads/UIThread.h"
#include "threads/WaiterThread.h"

int main(int argc, char *argv[]) {
    srand((unsigned int) time(NULL));
    auto options = parseArgs(argc, argv);

    if (options.find('h') != options.end()) {
        viewhelp();
        return 0;
    }

    if (!checkArgs(options)) {
        return -1;
    };

    cookingTime = options.find('s')->second;
    bufferSize = options.find('b')->second;
    tables = Tables(options.find('t')->second);
    waiters = Waiters(options.find('w')->second);
    cooks = Cooks(options.find('c')->second);
    clients = Clients(options.find('m')->second);
    buffer = Buffer(options.find('b')->second);

    std::thread WaiterThreads[options.find('w')->second];
    std::thread CookThreads[options.find('c')->second];
    std::thread ClientThreads[options.find('m')->second];
    std::thread UIThread_t;

    /// I N I T
    initscr();
    noecho();
    nodelay(stdscr, TRUE);
    curs_set(FALSE);
    use_default_colors();
    initColor();

    /// P R O G R A M
    int width = 0, height = 0;
    getmaxyx(stdscr, height, width);

    float RestScale = 0.75;
    float CooksWinRate = 0.4;
    float BufferWinRate = 0.1;
    float RoomWinRate = 0.7;

    CooksWindow = newwin((int) (height * RestScale * CooksWinRate),
                         (int) (width * RestScale), 0, 0);
    BufferWindow = newwin((int) (height * RestScale * BufferWinRate),
                          (int) (width * RestScale),
                          (int) (height * RestScale * CooksWinRate), 0);
    RoomWindow = newwin((int) (height * RestScale * RoomWinRate), (int) (width * RestScale),
                   (int) (height * RestScale * (CooksWinRate + BufferWinRate) - 1), 0);
    ClientsWindow = newwin(
            (int) (height -
                   height * RestScale * (CooksWinRate + RoomWinRate + BufferWinRate) +
                   2),
            (int) (width * RestScale),
            (int) (height * RestScale * (CooksWinRate + BufferWinRate + RoomWinRate) -
                   1),
            0);
    LogWindow = newwin(height, (int) (width - width * RestScale), 0,
                       (int) (width * RestScale + 1));

    UIThread_t = std::thread(UIThread, height);

    for (auto wc = 0; wc < waiters.getSize(); wc++) {
        WaiterThreads[wc] = std::thread(WaiterThread, &waiters.getWaiter(wc));
    }

    for (auto clc = 0; clc < clients.getSize(); clc++) {
        int eatingTime = (int) (5 + getRandomFloat() * 5);
        ClientThreads[clc] =
                std::thread(ClientThread, &clients.getClient(clc), eatingTime);
    }

    for (auto ckc = 0; ckc < cooks.getSize(); ckc++) {
        int cookingTimeInd = (int) (cookingTime + getRandomFloat() * cookingTime);
        CookThreads[ckc] =
                std::thread(CookThread, &cooks.getCook(ckc), cookingTimeInd);
    }

    /// E N D
    UIThread_t.join();

    for (auto wc = 0; wc < waiters.getSize(); wc++) {
        WaiterThreads[wc].join();
    }

    for (auto clc = 0; clc < clients.getSize(); clc++) {
        ClientThreads[clc].join();
    }

    for (auto ckc = 0; ckc < cooks.getSize(); ckc++) {
        CookThreads[ckc].join();
    }

    endwin();

    ///
    return 0;
}
