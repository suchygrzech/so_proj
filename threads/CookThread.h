#pragma once

#include "../headers/globals.h"
#include <string>
#include <unistd.h>

void timer(Cook *cook, int cookingTime);

void CookThread(Cook *cook, int cookingTime) {
    Table *table = &Table::None;

    while (!exiting) {
        switch (cook->state) {
            case Cook::IDLE:
                TablesMUTEX.lock();
                table = &tables.getTable(Table::WAIT_FOR_PREP);
                if (table != &Table::None) {
                    cook->next_state();
                    table->next_state(); // -> PREPARING
                }
                TablesMUTEX.unlock();
                break;

            case Cook::COOKING:
                timer(cook, cookingTime);

                TablesMUTEX.lock();
                table->next_state(); // -> WAIT_FOR_SERVICE
                TablesMUTEX.unlock();

                BufferMUTEX.lock();
                buffer.increment_buffer();
                BufferMUTEX.unlock();

                cook->next_state();
                cook->progress = 0;
                table = &Table::None;
                break;
        }
    }
}

void timer(Cook *cook, int cookingTime) {
    int ThreadWait = 100;
    long TimeInMs = 1000 * cookingTime;

    for (long t = 0; t < (long) TimeInMs / ThreadWait; t++) {
        if (exiting) {
            break;
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(ThreadWait));
        cook->progress = (float) t * 100 / (TimeInMs / ThreadWait);
    }
    cook->progress = 100;
}