#pragma once

#include "../headers/globals.h"
#include <unistd.h>

void timer(Waiter *waiter, int time);

void WaiterThread(Waiter *waiter) {
    Table *table = &Table::None;

    while (!exiting) {
        switch (waiter->state) {
            case Waiter::IDLE:
                std::this_thread::sleep_for(std::chrono::milliseconds(500));
                waiter->coord = waiter->bcoord;

                BufferMUTEX.lock();
                TablesMUTEX.lock();

                table = &tables.getTable(Table::WAIT_FOR_SERVICE);

                if (table != &Table::None && !buffer.isEmpty()) {
                    table->lock = true;
                    waiter->changeState(Waiter::SERVING);
                    BufferMUTEX.unlock();
                    TablesMUTEX.unlock();
                    continue;
                }
                BufferMUTEX.unlock();

                table = &tables.getTable(Table::WAIT_FOR_CLEAN);

                if (table != &Table::None) {
                    table->lock = true;
                    waiter->changeState(Waiter::CLEANING);
                    TablesMUTEX.unlock();
                    continue;
                }

                TablesMUTEX.unlock();
                break;

            case Waiter::SERVING:
                std::this_thread::sleep_for(std::chrono::milliseconds(500));
                BufferMUTEX.lock();
                buffer.decrement_buffer();
                BufferMUTEX.unlock();

                waiter->coord = std::make_pair(table->coord.first, table->coord.second);

                timer(waiter, 2);

                TablesMUTEX.lock();
                table->lock = false;
                table->next_state(); // -> BUSY
                TablesMUTEX.unlock();

                waiter->next_state();
                table = &Table::None;
                break;

            case Waiter::CLEANING:

                TablesMUTEX.lock();
                table->next_state();
                TablesMUTEX.unlock();

                waiter->coord = std::make_pair(table->coord.first, table->coord.second);

                timer(waiter, 10);

                TablesMUTEX.lock();
                table->lock = false;
                table->next_state();
                TablesMUTEX.unlock();

                waiter->next_state();
                table = &Table::None;
                break;
        }
    }
}

void timer(Waiter *waiter, int time) {

    int ThreadWait = 100;
    long TimeInMs = 1000 * time;

    for (long t = 0; t < TimeInMs / ThreadWait; t++) {
        if (exiting) {
            break;
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(ThreadWait));
        waiter->progress = (float) t * 100 / (TimeInMs / ThreadWait);
    }
    waiter->progress = 100;
}