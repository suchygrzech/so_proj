#pragma once

#include <ncurses.h>

namespace FIG {

    void drawRect(WINDOW *wnhdl, int x, int y, int width, int height) {

        if (width == 1 && height == 1) {
            mvwaddch(wnhdl, y, x, ACS_ULCORNER);
            waddch(wnhdl, ACS_URCORNER);
            mvwaddch(wnhdl, y + 1, x, ACS_LLCORNER);
            waddch(wnhdl, ACS_LRCORNER);
            return;
        }

        mvwaddch(wnhdl, y, x, ACS_ULCORNER);

        for (auto cc = 0; cc < width; cc++) {
            waddch(wnhdl, ACS_HLINE);
        }

        waddch(wnhdl, ACS_URCORNER);

        for (auto rc = 0; rc < height; rc++) {
            mvwaddch(wnhdl, y + 1 + rc, x, ACS_VLINE);
            mvwaddch(wnhdl, y + 1 + rc, x + width + 1, ACS_VLINE);
        }

        mvwaddch(wnhdl, y + height + 1, x, ACS_LLCORNER);

        for (auto cc = 0; cc < width; cc++) {
            waddch(wnhdl, ACS_HLINE);
        }

        waddch(wnhdl, ACS_LRCORNER);
    }

    void drawHorLine(WINDOW *wnhdl, int x, int y, int length, int sign) {

        move(y, x);

        for (auto cc = 0; cc < length; cc++) {
            waddch(wnhdl, sign);
        }
    }

    void drawVerLine(WINDOW *wnhdl, int x, int y, int length, int sign) {

        for (auto rc = 0; rc < length; rc++) {
            mvwaddch(wnhdl, y + rc, x, sign);
        }
    }
}