#pragma once

#include <ncurses.h>
#include <vector>

struct Waiter {
    static int IDs;
    static Waiter None;
    enum STATE {
        IDLE, SERVING, CLEANING
    };

    int ID = -1;
    STATE state = IDLE;
    int activeTable = -1;
    float progress = 0.0;

    std::pair<int, int> coord;
    std::pair<int, int> bcoord;

    Waiter() : ID(Waiter::IDs) {
        this->coord = bcoord = std::pair<int, int>(0, 0);
        Waiter::IDs++;
    }

    Waiter(int wid) { this->ID = wid; }

    void next_state() {
        switch (state) {
            case IDLE:
                state = IDLE;
                break;
            case SERVING:
                state = IDLE;
                break;
            case CLEANING:
                state = IDLE;
                break;
        }
    }

    void changeState(Waiter::STATE state) { this->state = state; }

    void redraw(WINDOW *RoomWindow) {
        wattron(RoomWindow, COLOR_PAIR(3) | A_BOLD);
        if (state == IDLE) {
            mvwaddch(RoomWindow, bcoord.second, bcoord.first, 'o');

        } else {
            mvwaddch(RoomWindow, coord.second - 1, coord.first, 'o');
        }
        if (state == CLEANING) {
            mvwprintw(RoomWindow, coord.second + 1, coord.first - 1, "%3.0f%%",
                      this->progress);
        }
        wattroff(RoomWindow, COLOR_PAIR(3) | A_BOLD);
    }
};

int Waiter::IDs = 0;
Waiter Waiter::None(-1);

class Waiters {
    std::vector <Waiter> waiters;

public:
    Waiters() {}

    Waiters(int count) { waiters = std::vector<Waiter>(count); }

    size_t getSize() { return waiters.size(); }

    std::vector <Waiter> &getWaiters() { return waiters; }

    Waiter &getWaiter(Waiter::STATE state) {
        for (Waiter &waiter : waiters) {
            if (waiter.state == state) {
                return waiter;
            }
        }
        return Waiter::None;
    }

    Waiter &getWaiter(int wid) {
        for (Waiter &waiter : waiters) {
            if (waiter.ID == wid) {
                return waiter;
            }
        }
        return Waiter::None;
    }

    void redraw(WINDOW *RoomWindow) {
        for (Waiter &waiter : waiters) {
            waiter.redraw(RoomWindow);
        }
    }
};