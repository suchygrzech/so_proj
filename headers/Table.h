#pragma once

#include "figures.h"
#include <utility>
#include <vector>

struct Table {
    static int IDs;
    static Table None;
    enum STATE {
        FREE,
        WAIT_FOR_PREP,
        PREPARING,
        WAIT_FOR_SERVICE,
        BUSY,
        WAIT_FOR_CLEAN,
        CLEANING
    };

    int ID = -1;
    std::pair<int, int> coord;
    STATE state = FREE;
    bool lock = false;

    Table() : ID(Table::IDs) {
        coord = std::pair<int, int>(0, 0);
        Table::IDs++;
    }

    Table(int id) : ID(id) {}

    Table(const Table &table) {
        this->ID = table.ID;
        this->coord = table.coord;
        this->state = table.state;
        this->lock = table.lock;
    }

    void next_state() {
        std::unique_lock <std::mutex> lock;
        switch (this->state) {
            case FREE:
                state = WAIT_FOR_PREP;
                break;
            case WAIT_FOR_PREP:
                state = PREPARING;
                break;
            case PREPARING:
                state = WAIT_FOR_SERVICE;
                break;
            case WAIT_FOR_SERVICE:
                state = BUSY;
                break;
            case BUSY:
                state = WAIT_FOR_CLEAN;
                break;
            case WAIT_FOR_CLEAN:
                state = CLEANING;
                break;
            case CLEANING:
                state = FREE;
                break;
        }
    }

    void redraw(WINDOW *RoomWindow) {
        int pair = 0;
        switch (state) {
            case FREE:
                pair = 2;
                break;
            case WAIT_FOR_PREP:
                pair = 1;
                break;
            case PREPARING:
                pair = 4;
                break;
            case WAIT_FOR_SERVICE:
                pair = 4;
                break;
            case BUSY:
                pair = 3;
                break;
            case WAIT_FOR_CLEAN:
                pair = 5;
                break;
            case CLEANING:
                pair = 5;
                break;
        }

        wattron(RoomWindow, COLOR_PAIR(pair));
        FIG::drawRect(RoomWindow, this->coord.first, this->coord.second, 1, 1);
        wattroff(RoomWindow, COLOR_PAIR(pair));
    }
};

int Table::IDs = 0;
Table Table::None(-1);

class Tables {
private:
    std::vector <Table> tables;

public:
    Tables() {}

    Tables(int count) { this->tables = std::vector<Table>(count); }

    size_t getSize() { return tables.size(); }

    std::vector <Table> &getTables() { return tables; }

    Table &getTable(Table::STATE state) {
        for (Table &table : tables) {
            if (table.state == state && !table.lock)
                return table;
        }
        return Table::None;
    }

    Table &getTable(int tid) {
        for (Table &table : tables) {
            if (table.ID == tid) {
                return table;
            }
        }
        return Table::None;
    }

    void redraw(WINDOW *wnhdl) {
        for (Table &table : tables) {
            table.redraw(wnhdl);
        }
    }
};