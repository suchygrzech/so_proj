#pragma once

#include <cstring>
#include <ncurses.h>
#include <sstream>
#include <string>
#include <vector>

struct Client {
    static int IDs;
    static Client None;
    enum STATE {
        OUTSIDE, WAITING, EATING, EXITING
    };
    int ID = -1;
    STATE state = OUTSIDE;
    std::pair<int, int> coord;

    int eatingTime = 0;
    float progress = 0.0;

    Client() {
        this->ID = Client::IDs++;
        this->coord = std::make_pair(0, 0);
    }

    Client(int id) {
        this->ID = id;
        this->coord = std::make_pair(0, 0);
    }

    void next_state() {
        switch (state) {
            case OUTSIDE:
                state = WAITING;
                break;
            case WAITING:
                state = EATING;
                break;
            case EATING:
                state = EXITING;
                break;
            case EXITING:
                break;
        }
    }

    void redraw(WINDOW *ClientWindow, WINDOW *RoomWindow) {
        if (state == Client::OUTSIDE)
            mvwaddch(ClientWindow, coord.second, coord.first, 'O');
        else
            mvwaddch(RoomWindow, coord.second, coord.first, 'O');
    }
};

int Client::IDs = 0;
Client Client::None(-1);

class Clients {

private:
    std::vector <Client> clients;

public:

    Clients() {}

    Clients(int count) { this->clients = std::vector<Client>(count); }

    size_t getSize() { return clients.size(); }

    Client &getClient(int clid) {
        for (Client &client : clients) {
            if (client.ID == clid) {
                return client;
            }
        }
        return Client::None;
    }

    Client &getClient(Client::STATE state) {
        for (Client &client : clients) {
            if (client.state == state) {
                return client;
            }
        }
        return Client::None;
    }

    void eraseClient(Client *client) {
        for (auto ci = 0; ci < clients.size(); ci++) {
            if (clients.at(ci).ID == client->ID) {
                clients.erase(clients.begin() + client->ID);
                break;
            }
        }
    }

    std::vector <Client> &getClients() { return clients; };

    void redraw(WINDOW *ClientsWindow, WINDOW *RoomWindow) {
        for (Client &client : clients) {
            client.redraw(ClientsWindow, RoomWindow);
            if (client.state == Client::EATING) {
                mvwprintw(RoomWindow, client.coord.second - 2, client.coord.first - 1,
                          "%3.f%%", client.progress);
            }
        }
    }
};