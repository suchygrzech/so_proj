#pragma once
#include "inits.h"
#include <sstream>
#include <string>
#include <vector>

class Log {
private:
    std::vector<std::string> logs;
    int max_logs;

public:

    Log(int max_logs) : max_logs(max_logs) {}

    void Add(std::string log) {
        if (logs.size() > max_logs)
            logs.pop_back();
        logs.insert(logs.begin(), log);
    }

    std::vector<std::string> getLogs() { return logs; }

    size_t size() { return logs.size(); }

    void changeSize(int newsize) {
        this->max_logs = newsize;
        logs.clear();
    }

    static std::string Serialize(Client &client) {
	std::ostringstream o;
	o << "CL[" << client.ID << "]: ";
        switch (client.state) {
            case Client::STATE::OUTSIDE:
                o << "Outside";
                break;
            case Client::STATE::WAITING:
                o <<  "Waiting";
                break;
            case Client::STATE::EATING:
                o << "Eating";
                break;
            case Client::STATE::EXITING:
                o << "Exiting";
                break;
        }

        return o.str();
    }

    static std::string Serialize(Table &table) {
        std::ostringstream o;
	o << "T[" <<  table.ID << "]: ";
        switch (table.state) {
            case Table::STATE::FREE:
                o <<  "Free";
                break;
            case Table::STATE::WAIT_FOR_PREP:
                o << "Wait for prep";
                break;
            case Table::STATE::PREPARING:
                o <<  "Prep a dish";
                break;
            case Table::STATE::WAIT_FOR_SERVICE:
                o <<  "Wait for waiter";
                break;
            case Table::STATE::BUSY:
                o << "Busy";
                break;
            case Table::STATE::WAIT_FOR_CLEAN:
                o <<  "Wait for cleaning";
                break;
            case Table::STATE::CLEANING:
                o <<  "Cleaning";
                break;
        }

        return o.str();
    }

    static std::string Serialize(Waiter &waiter) {
	    std::ostringstream o;
	    o << "W[" <<  waiter.ID << "]: ";

        switch (waiter.state) {
            case Waiter::STATE::IDLE:
                o <<  "Idle";
                break;
            case Waiter::STATE::SERVING:
                o <<  "Serving a dish";
                break;
            case Waiter::STATE::CLEANING:
                o << "Cleaning a table";
                break;
        }

        return o.str();
    }

    static std::string Serialize(Cook &cook) {
        std::ostringstream o;
	o << "CO[" << cook.ID << "]: ";

        switch (cook.state) {
            case Cook::STATE::COOKING:
                o << "Cooking";
                break;
            case Cook::STATE::IDLE:
                o << "Idle";
                break;
        }

        return o.str();
    }

    static std::string Serialize(Buffer &buffer){
	    std::ostringstream o;
	    o << "Buffer: " << buffer.buffer;
        return o.str();
    }
};
