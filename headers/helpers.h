#pragma once

#include "inits.h"
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctype.h>
#include <map>
#include <random>
#include <string>
#include <vector>

std::random_device rd;
std::mt19937 gen(rd());
std::uniform_real_distribution<float> dis(0.5, 1.5);

void viewhelp() {
    {
        printf("Grzegorz Suszka, Systemy operacyjne 2 - PROJEKT\n");
        printf("\t\tSYMULACJA RESTAURACJI\n");
        printf("Program po uruchomieniu symuluje ruch w restauracji:\n");
        printf("Klienci wchodzą do środka restauracji, jeśli są wolne stoliki.");
        printf("Po tym jak wejdą do środka, kucharze zaczynają przygotowywać dania, które jeśli zostaną skończone, zostają rozniesione do odpowiednich stolików.");
        printf("Klienci jedzą swoje dania określony w programie czas, a następnie odchodzą od stolików.");
        printf("Stoliki po odejściu klienta wymagają sprzątnięcia, co robią kelnerzy.\n");

        printf("\nW czasue działania programu możliwa jest zmiana sposobu wyświetlania okna podgladu zmian stanów obiektów.");
        printf("\nKlawisze odpowiadające klasom obiektów:\n");
        printf("\t'1':\tKucharze\n");
        printf("\t'2':\tBufor\n");
        printf("\t'3':\tKlienci\n");
        printf("\t'4':\tKelnerzy\n");
        printf("\t'5':\tStoliki\n");

        printf("\n\tAby wyjść z programu, trzeba nacisnąć klawisz 'q' i poczekać aż wszystkie wątki zakończą swoje działanie\n");


        printf("Program przyjmuje obowiązkowe parametry:\n");

        printf("\t-t [5 - 60]:\tliczba stolików w restauracji\n");
        printf("\t-w [5 - 60]:\tliczba kelnerów\n");
        printf("\t-c [5 - 60]:\tliczba kucharzy\n");
        printf("\t-s [5 - 60]:\tśredni czas przygotowywania posiłku przez kucharza w sekundach [s]\n");
        printf("\t-b [5 - 60]:\twielkość bufora dań\n");
    }

    {
        printf("\nDodatkowo program przyjmuje parametry:\n");

        printf("\t-m [liczba]:\tliczba klientów\n");
        printf("\t-h :\t\twyświetlenie tej pomocy\n");
    }
}

void checkIfCorr(std::map<char, int> args) {

    int cookingTime = args.find('s')->second;
    int bufferSize = args.find('b')->second;
    int tablesCount = args.find('t')->second;
    int waitersCount = args.find('w')->second;
    int cooksCount = args.find('c')->second;
    int clientsCount = args.find('m')->second;

    if (cookingTime > 60 || cookingTime <= 0) args.find('s')->second = 60;
    if (bufferSize > 60 || bufferSize <= 0) args.find('b')->second = 60;
    if (tablesCount > 60 || tablesCount <= 0) args.find('t')->second = 60;
    if (waitersCount > 60 || waitersCount <= 0) args.find('w')->second = 60;
    if (cooksCount > 60 || cooksCount <= 0) args.find('c')->second = 60;
    if (clientsCount > 256 || clientsCount <= 0) args.find('m')->second = 256;

}

std::map<char, int> parseArgs(int argc, char *argv[]) {
    std::map<char, int> values;
    for (int c = 1; c < argc; c++) {
        if (argv[c][0] == '-' && isalpha(argv[c][1])) {
            if (tolower(argv[c][1]) == 'h') {
                values.insert(std::pair<char, int>(tolower(argv[c][1]), 0));
                return values;
            }
            c++;
            if (c < argc) {
                values.insert(
                        std::pair<char, int>(tolower(argv[c - 1][1]), atoi(argv[c])));
            }
        }
    }

    values.insert(std::pair<char, int>('m', 200));
    return values;
}

bool checkArgs(std::map<char, int> &args) {
    bool error = false;
    std::vector<char> neededArgs{'t', 'w', 'c', 's', 'b'};
    for (auto arg : neededArgs) {
        if (args.find(arg) == args.end()) {
            printf("There is no '%c' argument. Please, run this program with all "
                           "needed args.\n",
                   arg);
            return false;
        }
    }
    return true;
}

float getRandomFloat() { return dis(gen); }